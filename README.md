# Scripts

Some scripts that I hackily put together.

## GitLab-specific

### [create-issues.py](create-issues.py)

Create multiple GitLab issues using [GitLab API](https://docs.gitlab.com/ee/api/issues.html).

Issue titles and descriptions are specified as a Python dictionary:

```py
content = {

    "Title 1": """Line 1

    Line 2""",
    "Title 2": """Line 1

    Line 2""",
    }
```

### [pull-all-gitlab-repos.sh](pull-all-gitlab-repos.sh)

Regardless of where it's run from:

- Enters each repository in the specified folder (here `~/dev/gitlab`).
- Changes to main branch.
- Pull all remotes.
- Installs Bundle and Yarn dependencies.

### [serve-docs.sh](serve-docs.sh)

Script for building and previewing locally GitLab repositories.
It determines where you are and builds either the website or the docs.
If you run it from any other directory, it builds the docs anyway.

Simultaneously, it opens the live preview at a default port, using `xdg-open` (because Linux 🐧; change to `open` for Mac).

[Linux-specific] When done, sends a notification using `notify-send`.

## General

### [are-you-sure.sh](are-you-sure.sh)

Show a GUI dialog asking "Are you sure?".
Clicking "Yes" exits with `0` (success).
Clicking "No" exits with `1` (error).

Uses `zenity`, a GNOME library.
[For Mac, get it through Homebrew or use an alternative](https://apple.stackexchange.com/questions/73613/is-there-a-mac-osx-equivalent-of-zenity).

### [restart-confirm.sh](restart-confirm.sh)

Run `are-you-sure.sh` and on success, reboot.
Uses XFCE's `xfce4-session-logout` command.

### [shut-down-confirm.sh](shut-down-confirm.sh)

Run `are-you-sure.sh` and on success, shut down.
Uses XFCE's `xfce4-session-logout` command.

### [suspend-confirm.sh](suspend-confirm.sh)

Run `are-you-sure.sh` and on success, suspend.
Uses XFCE's `xfce4-session-logout` command.
